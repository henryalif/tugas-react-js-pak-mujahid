import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const nama = " henry " ;
  const no_absen = 10;

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
          <p>
            Tugas React 2 ( App )
          </p>
          <h1> Nama saya {nama} </h1>
          <h2> Absen {no_absen} </h2>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}


const AppVar = ()=>{
  const nama = " henry ";
  const no_absen = 10 ;

  return(
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
           <p>
              Tugas React 2 ( Ini AppVar )
          </p>
          <h1> Nama saya {nama} </h1>
          <h2> Absen {no_absen} </h2>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

class AppClass extends React.Component {
  constructor(props){
    super(props);
    this.nama = "henry";
    this.no_absen = 10 ;
  }

    render(){
      return(
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
                Tugas React 2 ( Ini AppClass )
            </p>
            <h1> Nama saya {this.nama} </h1>
            <h2> Absen {this.no_absen} </h2>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

    export {
    App,
    AppVar,
    AppClass
    };
